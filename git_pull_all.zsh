#!/bin/zsh
set -ueo extendedglob
GITDIR="${HOME}/GIT"


function git_pull_branch {
    local branch="$1"
    echo " ** git pull ${branch} **"
    git checkout ${branch}
    git pull
    git clean -d -f
}


function git_pull {
    local folder="$1"
    cd ${folder}
    echo
    echo "*** git pull & clean ${folder} ***"
    branches=($(git branch --list --no-color --format '%(refname:short)'))
    echo "    branches: ${branches}" | tr '\n' ', ' | sed 's/,$//'
    for br in ${branches}; do
	 git_pull_branch ${br}
    done
    git clean -d -f
    echo "*** DONE: git pull & clean ${folder} :) ***"
}

echo "GITDIR = ${GITDIR}"

echo "Deleting target folders"
rm -rf $(find ${GITDIR} -d -iname "target")

for gitIgnore in ${GITDIR}/*/.gitignore
do
  git_pull $(dirname ${gitIgnore} )
done

cd ${GITDIR}

echo "**************"
echo "**** Done ****"
echo "**************"

du -hsc *

read
exit 0

# eof
