#!/usr/bin/env zsh
WORKDIR=${0:h}
LOGDIR="/home/klt/log"
OUTFILE="${LOGDIR}/${0:t:r}.out"
ERRFILE="${LOGDIR}/${0:t:r}.err"
SERVICES=($*)

source /home/klt/Zaclys/Documents/Computer/SetUp/klt_zshrc

exec 1>${OUTFILE} 2>${ERRFILE}

logg "START $0 $*: running as $(whoami)"

function activateService {
    systemctl enable ${SERVICE}
    sleep 3
    systemctl start ${SERVICE}
    sleep 3
    if [[ ${SERVICE} == bluetooth ]]; then
        rfkill unblock ${SERVICE}
        sleep 3
        rfkill
        sleep 3
    fi
    systemctl status ${SERVICE}
    echo ""
    if [[ ${SERVICE} == bluetooth ]]; then
        echo ""
        bt-device -l
        echo ""
        logg "! You may need to switch OFF and ON your ${SERVICE} device !"
    fi
}

function deactivateService {
    logg "systemctl stop ${SERVICE}"
    systemctl stop ${SERVICE}
    sleep 3
    logg "systemctl disable ${SERVICE}"
    systemctl disable ${SERVICE}
    sleep 3
    systemctl status ${SERVICE}
}

function toggleService {
    SERVICE=$1
    logg "SERVICE = ${SERVICE}"
    STATUS=$(systemctl is-active ${SERVICE})
    logg "STATUS  = ${STATUS}"
    echo ""

    if [[ $STATUS == "active" ]]; then
        deactivateService
    else
        activateService
    fi

    echo ""
    logg "SERVICE = ${SERVICE}"
    STATUS=$(systemctl is-active ${SERVICE})
    logg "STATUS  = ${STATUS}"
    echo ""
}

for svc in ${SERVICES}; do toggleService ${svc} ; done

logg "DONE $0 $*"

xmessage -file ${OUTFILE}

exit $?
